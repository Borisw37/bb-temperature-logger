# ========= IMPORTS =============
import os
import sys
from time import sleep
from Adafruit_BBIO.SPI import SPI
import logging

# =========== Initialization Scripts =============
print "Server _ON_"
print "Open SPI"
spi = SPI(0,0) #(bus, device)
spi.mode=0 #[CPOL][CPHA] values from 0..3
spi.msh=2000000
spi.open(0,0)
APP_ROOT = os.path.dirname(os.path.realpath(sys.argv[0])) #gets current directory where script resides


def twos_comp(val, bits): 
    #compute the 2's compliment of int value val
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val                         # return positive value as is
    
    
def spi_read_LM74():
    #read SPI data from LM74 sensor
    #clock - P9_22
    #data out - P9_18 (not connected)
    #data in - P9_21
    #chip select - P9_17
    global spi #use the globally defined "spi", prevents from having to open/close spi on every read
    read_val_list=spi.xfer2([0,0]) #clock out two bytes of pulses. Read data in. Data out is not connected.
    #print(format(read_val_list[0], '#010b'),format(read_val_list[1], '#010b'))
    read_val=(read_val_list[0] << 8) | read_val_list[1] # concatenate into a single 16bit value
    read_val = (read_val >> 3) & 0x1FFF #only 13 upper bits of data are temperature data
    read_val = twos_comp(read_val,13) 
    temperature  = read_val * 0.0625;
    print "Temp = ", temperature
    return temperature
    
def main():
    #logging.basicConfig(format='%(asctime)s;%(message)s', filename=APP_ROOT+'/temp.log', level=logging.INFO)
    logging.basicConfig(format='%(asctime)s;%(message)s',
                        datefmt='%Y-%m-%dT%H:%M:%SZ', #UTC format for easier use in Java Script
                        filename=APP_ROOT+'/temp.log',
                        level=logging.INFO)
    
    while(1):
        logging.info(spi_read_LM74()) #read temperature value and write to log file
        sleep(1) #sleep for one second
        pass
        
if __name__ == '__main__':
    main()


