 #-------------------------------------------------------------------------------
# Name: temp_webserver
# Purpose: Temperature logger demo using LM74 SPI sensor
#
# Author: Boris O.     
#
# Created:     02/10/2015
#-------------------------------------------------------------------------------
#NOTES:


# ========= IMPORTS =============
import cherrypy
import os
import simplejson
import sys
from time import sleep
#import string

WEB_ROOT = os.path.dirname(os.path.realpath(sys.argv[0])) #Find the directory where the application is launched from

# ============== Main Server Object ======
class TempSensor(object):
# =========== Dictionary of Script commands {Command: number of parameters,...} ============= commands are all lower-case only.
    def __init__(self): 
        pass
        
    @cherrypy.expose
    def GetTempDataJSON(self, LastRecordLoaded):
        LastRecordLoaded = int(LastRecordLoaded)
        print ">GetTempDataJSON " + str(LastRecordLoaded)
        DataList = []
        fTempLog = open(WEB_ROOT+'/temp.log', 'r')
        for lineN, line in enumerate(fTempLog): #Temperature log data is stored in "date;value" format
            if lineN > LastRecordLoaded:
                KeyVal = line.split(";") #split by ";" designator
                KeyVal[1] = KeyVal[1].strip('\n')
                DataList.append((KeyVal[0], KeyVal[1]))
        cherrypy.response.headers['Content-Type'] = 'application/json' # label return structure as 'json'
        return simplejson.dumps(DataList)
        
    @cherrypy.expose
    def ClearTempLog(self):
        with open(WEB_ROOT+'/temp.log', 'w'):
            pass
            
            
# SERVER STUFF - DO NOT TOUCH UNLESS ABSOLUTELY NECESSARY
cherrypy.server.socket_port = 80
cherrypy.server.socket_host = '0.0.0.0'
#cherrypy.thread_pool_max = 1
conf = {'/':
	{
		'tools.staticdir.on': True,
		'tools.staticdir.dir': WEB_ROOT,
		'tools.staticdir.index': 'index.html',
	}
}

cherrypy.quickstart(TempSensor(), config=conf)


